Feature: create a random pet

  Scenario: get 200
    When user requests for creating a random pet
    Then 200 code is returned
    Then pet is created

  Scenario: get 405
    When user requests for a pet with invalid body
    Then 400 code is returned
    Then "bad input" message is returned