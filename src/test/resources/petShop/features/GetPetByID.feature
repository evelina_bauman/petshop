Feature: get pet by ID
  Background:
    Given 5 random pets are created

  Scenario: get 200
    When user requests for a random existing pet
    Then 200 code is returned
    Then pet is returned

  Scenario Outline: get 400 with different sets of invalid IDs
    When user requests for a pet with ID <ID>
    Then 404 code is returned
    Then "Invalid ID supplied" message is returned
    Examples:
    | ID |
    | 'asdfsdf'|
    | ' ' |
    | '!@#$:"""'  |

  Scenario: get 404
    Given pet with ID '123453453453223453' does not exist
    When user requests for a pet with ID '123453453453223453'
    Then 404 code is returned
    Then 'Pet not found' message is returned