Feature: update pet
  Background:
    Given 5 random pets are created

  Scenario: update pet name
    When user requests to update an existing pet with a new random name
    Then 200 code is returned
    Then pet is updated

  Scenario: update all pet fields
    When user requests to update an existing pet with all new random values
    Then 200 code is returned
    Then pet is updated

  Scenario: update pet name several times
    When user requests to update an existing pet with a new random name 5 times
    Then 200 code is returned
    Then pet is updated