Feature: delete pet

  Scenario: delete pet
    Given 1 random pets are created
    When user requests to delete a random pet
    Then 200 code is returned
    Then random pet is deleted

  Scenario: delete non-existing pet
    Given pet with ID '123453453453223453' does not exist
    When user requests to delete a pet with ID '123453453453223453'
    Then 404 code is returned
    Then 'Pet not found' message is returned
