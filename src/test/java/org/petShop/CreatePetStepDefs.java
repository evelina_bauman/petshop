package org.petShop;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.petShop.model.Pet;
import utils.PetCreator;

import static org.petShop.generator.PetGenerator.generatePet;
import static utils.RestUtil.doPost;

public class CreatePetStepDefs extends AbstractSteps {

    @Then("pet is created")
    public void checkCreatedPet() {
        Pet generatedPet = testContext().getResponse().as(Pet.class);
        Assert.assertEquals(testContext().getInitialPet(), generatedPet);
    }

    @When("user requests for a pet with invalid body")
    public void createPetInvalidBody() {
        String jsonPet = "invalid JSON";
        doPost("pet", jsonPet);
    }

    @When("user requests for creating a random pet")
    public void createRandomPet() {
        testContext().setInitialPet(generatePet());
        Pet createdPet = PetCreator.createRandomPet(testContext().getInitialPet());
        Assert.assertEquals(createdPet, testContext().getInitialPet());
    }
}
