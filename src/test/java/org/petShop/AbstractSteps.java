package org.petShop;

import org.jdbi.v3.core.Jdbi;
import utils.TestContext;

import static utils.TestContext.CONTEXT;

public abstract class AbstractSteps {

    public static Jdbi connection = testContext().getConnection();

    public static TestContext testContext() {
        return CONTEXT;
    }


}
