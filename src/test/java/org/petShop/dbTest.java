package org.petShop;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Query;
import org.petShop.model.Category;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class dbTest extends AbstractSteps {
    public static void main(String[] args) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
//        testContext().getConnection().useHandle(handle -> {
//            handle.execute("insert into category values (2, 'boobenchick')");
//        });

//        List<Map<String, Object>> a = connection.withHandle(handle -> {
//            Query query = handle.createQuery("select * from category");
//            return query.mapToMap().list();
//        });

        List<Category> a = connection.withHandle(handle -> handle.createQuery("select * from category")
                .map((rs, ctx) -> new Category(rs.getInt("category_id"), rs.getString("name"))).list());
    }
}
