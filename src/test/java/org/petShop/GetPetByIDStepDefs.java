package org.petShop;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.petShop.model.Pet;
import utils.PetCreator;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.petShop.generator.PetGenerator.generatePet;
import static utils.RandomizerUtil.generateRandomInteger;
import static utils.RestUtil.doGet;

public class GetPetByIDStepDefs extends AbstractSteps {

    @Given("{int} random pets are created")
    public void randomPetsAreCreated(int amount) {
        ArrayList<Pet> petCollection = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            testContext().setInitialPet(generatePet());
            petCollection.add(PetCreator.createRandomPet(testContext().getInitialPet()));
            Assert.assertEquals(petCollection.get(i), testContext().getInitialPet());
        }
        testContext().setPetCollection(petCollection);
        Assert.assertEquals(amount, petCollection.size());
    }

    @When("user requests for a random existing pet")
    public void getRandomPet() {
        testContext().setRandItemNumber(generateRandomInteger(testContext().getPetCollection().size()-1));
        doGet("pet/" + testContext().getPetCollection().get(testContext().getRandItemNumber()).getId());
    }

    @When("user requests for a pet with ID {string}")
    public static void getPetByID(String id) {
        doGet("pet/" + id);
    }

    @Then("pet is returned")
    public void verifyRandomPet() {
        Pet pet = testContext().getResponse().as(Pet.class);
        Assert.assertEquals(testContext().getPetCollection().get(testContext().getRandItemNumber()), pet);
    }

    @Then("{int} code is returned")
    public void checkStatusCode(int statusCode) {
        testContext().getResponse().then().assertThat().statusCode(statusCode);
    }

    @Then("{string} message is returned")
    public void verifyErrorMessage(String errorMessage) {
        testContext().getResponse().then().assertThat()
                .body("message", equalTo(errorMessage));
    }
}
