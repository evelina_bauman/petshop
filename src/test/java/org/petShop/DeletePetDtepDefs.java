package org.petShop;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.petShop.model.Pet;

import static org.petShop.GetPetByIDStepDefs.getPetByID;
import static utils.RandomizerUtil.generateRandomInteger;
import static utils.RestUtil.doDelete;
import static utils.RestUtil.doGet;

public class DeletePetDtepDefs extends AbstractSteps {

    @When("user requests to delete a pet with ID {string}")
    public void deletePetByID(String id) {
        doDelete(id);
    }

    @When("pet with ID {string} does not exist")
    public void checkPetDoesNotExist(String id) {
        doGet("pet/".concat(id));
        testContext().getResponse().then().assertThat().statusCode(404);
    }

    @When("user requests to delete a random pet")
    public void deleteRandomPet() {
        testContext().setRandItemNumber(generateRandomInteger(testContext().getPetCollection().size()-1));
        deletePetByID(String.valueOf(testContext().getPetCollection().get(testContext().getRandItemNumber()).getId()));
    }

    @Then("random pet is deleted")
    public void verifyRandomPetDeleted(){
        getPetByID(String.valueOf(testContext().getPetCollection().get(testContext().getRandItemNumber()).getId()));
        Pet pet = testContext().getResponse().as(Pet.class);
        Assert.assertNotEquals(testContext().getPetCollection().get(testContext().getRandItemNumber()), pet);
    }
}
