package org.petShop;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.petShop.generator.PetGenerator;
import org.petShop.model.Pet;

import static org.petShop.generator.PetGenerator.generateName;
import static utils.JsonUtil.getJsonFromObject;
import static utils.RandomizerUtil.generateRandomInteger;
import static utils.RestUtil.doPut;

public class UpdatePetStepDefs extends AbstractSteps {

    @When("user requests to update an existing pet with a new random name")
    public void updateRandomPetName() {
        testContext().setRandItemNumber(generateRandomInteger(testContext().getPetCollection().size()-1));
        testContext().getPetCollection().get(testContext().getRandItemNumber()).setName(generateName());
        String jsonPet = getJsonFromObject(testContext().getPetCollection().get(testContext().getRandItemNumber()));
        doPut("pet", jsonPet);
    }

    @Then("pet is updated")
    public void verifyUpdatedPet() {
        Pet pet = testContext().getResponse().as(Pet.class);
        Pet originalPet = testContext().getPetCollection().stream()
                .filter(p -> p.getId() == pet.getId()).findFirst()
                .orElseThrow(()-> new RuntimeException("Pet for verification is not found"));
        Assert.assertEquals(originalPet, pet);
    }

    @When("user requests to update an existing pet with all new random values")
    public void updateRandomPetFull() {
        testContext().setRandItemNumber(generateRandomInteger(testContext().getPetCollection().size()-1));
        Pet petToUpdate = testContext().getPetCollection().get(testContext().getRandItemNumber());
        petToUpdate.copyPetWithoutID(PetGenerator.generatePet());
        String jsonPet = getJsonFromObject(petToUpdate);
        doPut("pet", jsonPet);
    }

    @When("user requests to update an existing pet with a new random name {int} times")
    public void updatePetNameSeveralTime(Integer count) { //TODO add another method to update with the same value
        while (count > 0) {
            updateRandomPetName();
            count--;
        }
    }
}
