package utils;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.jdbi.v3.core.Jdbi;
import org.petShop.model.Pet;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.lang.ThreadLocal.withInitial;

public enum TestContext {

    CONTEXT;

    private static final String PAYLOAD = "PAYLOAD";
    private static final String REQUEST = "REQUEST";
    private static final String RESPONSE = "RESPONSE";

    private static final String PET_COLLECTION = "PET_COLLECTION";
    private static final String INITIAL_PET = "INITIAL_PET";

    private static final String RANDOM_ITEM_NUMBER = "RANDOM_ITEM_NUMBER";
    private final ThreadLocal<Map<String, Object>> testContexts = withInitial(HashMap::new);
    private Jdbi jdbi = Jdbi.create("jdbc:mysql://localhost:3306/petshop", "admin", "admin");


    public <T> T get(String name) {
        return (T) testContexts.get()
                .get(name);
    }

    public <T> T set(String name, T object) {
        testContexts.get()
                .put(name, object);
        return object;
    }

    public RequestSpecification getRequest() {
        if (null == get(REQUEST)) {
            set(REQUEST, given().log()
                    .all());
        }

        return get(REQUEST);
    }

    public Jdbi getConnection() {
        return jdbi;
    }

    public Response getResponse() {
        return get(RESPONSE);
    }

    public Response setResponse(Response response) {
        return set(RESPONSE, response);
    }

    public Pet getInitialPet() {
        return get(INITIAL_PET);
    }

    public Pet setInitialPet(Pet pet) {
        return set(INITIAL_PET, pet);
    }

    public int getRandItemNumber() {
        return get(RANDOM_ITEM_NUMBER);
    }

    public int setRandItemNumber(int randomNumber) {
        return set(RANDOM_ITEM_NUMBER, randomNumber);
    }

    public ArrayList<Pet> getPetCollection() {
        return get(PET_COLLECTION);
    }

    public ArrayList<Pet> setPetCollection(ArrayList<Pet> pets) {
        return set(PET_COLLECTION, pets);
    }

    public Object getPayload() {
        return get(PAYLOAD);
    }

    public <T> T getPayload(Class<T> clazz) {
        return clazz.cast(get(PAYLOAD));
    }

    public <T> void setPayload(T object) {
        set(PAYLOAD, object);
    }

    public void reset() {
        testContexts.get()
                .clear();
    }
}
