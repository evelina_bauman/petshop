package utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Paths;

public class FileUtil {

    public static <T> void writeObjectAsJsonToFile(T object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(Paths.get(pathGenerator(object)).toFile(), object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static <T> String pathGenerator(T object ){
        return String.format("src/test/resources/testData/%s_%d.json", object.getClass().getSimpleName(), System.currentTimeMillis());
    }
}
