package utils;

import org.petShop.model.Pet;

import static utils.FileUtil.writeObjectAsJsonToFile;
import static utils.JsonUtil.getJsonFromObject;
import static utils.RestUtil.doPost;

public class PetCreator {
    public static Pet createRandomPet(Pet pet) {
        String jsonPet = getJsonFromObject(pet);
        writeObjectAsJsonToFile(pet);

        return doPost("pet", jsonPet).as(Pet.class);
    }
}
