package utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestUtil {
    private static final String ENDPOINT_BASE = "https://petstore.swagger.io/v2/";

    public static Response doPost(String endpoint, String body) {
        final RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(body);
        Response response = request.post(ENDPOINT_BASE.concat(endpoint));
        TestContext.CONTEXT.setResponse(response);
        return response;
    }

    public static Response doGet(String endpoint) {
        final RequestSpecification request = RestAssured.given();
        Response response = request.get(ENDPOINT_BASE.concat(endpoint));
        TestContext.CONTEXT.setResponse(response);
        return response;
    }

    public static Response doPut(String endpoint, String body) {
        final RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(body);
        Response response = request.put(ENDPOINT_BASE.concat(endpoint));
        TestContext.CONTEXT.setResponse(response);
        return response;
    }

    public static Response doDelete(String endpoint) {
        final RequestSpecification request = RestAssured.given();
        Response response = request.delete(ENDPOINT_BASE.concat(endpoint));
        TestContext.CONTEXT.setResponse(response);
        return response;
    }
}
