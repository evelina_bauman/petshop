package utils;

import java.util.Random;

public class RandomizerUtil {
    public static int generateRandomInteger(int maxValue){
        if(maxValue == 0)
            return 0;
         return new Random().nextInt(maxValue);
    }
}
