package org.petShop.generator;

import com.github.javafaker.Faker;
import org.petShop.model.Category;
import org.petShop.model.Pet;
import org.petShop.model.Tag;

public class PetGenerator {
    static Faker faker = new Faker();

    public static Pet generatePet() {
        Pet randomPet = new Pet();
        randomPet.setId(faker.number().numberBetween(100000000, 999999999));
        randomPet.setCategory(generateCategory());
        randomPet.setName(faker.artist().name());
        randomPet.setPhotoUrls(new String[]{faker.company().url()});
        randomPet.setStatus("available");
        randomPet.setTags(new Tag[]{generateTag()});
        return randomPet;
    }

    public static String generateName() {
        return faker.artist().name();
    }

    public static Category generateCategory() {
        int categoryID = faker.number().numberBetween(100000000, 999999999);
        String categoryName = faker.animal().name();
        return new Category(categoryID, categoryName);
    }

    public static Tag generateTag() {
        int tagID = faker.number().numberBetween(100000000, 999999999);
        String tag = faker.ancient().god();
        return new Tag(tagID, tag);
    }
}
