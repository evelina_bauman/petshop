package org.petShop.model;

import lombok.Data;

import java.util.Arrays;
import java.util.Objects;

@Data
public class Pet {
    private int id;
    private Category category;
    private String name;
    private String[] photoUrls;
    private Tag[] tags;
    private String status;

    public void copyPetWithoutID(Pet pet){
        this.setCategory(pet.category);
        this.setName(pet.name);
        this.setPhotoUrls(pet.photoUrls);
        this.setTags(pet.tags);
        this.setStatus(pet.status);
    }

    public void copyPetID(Pet pet){
        this.setId(pet.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return id == pet.id &&
                Objects.equals(category, pet.category) &&
                name.equals(pet.name) &&
                Arrays.equals(photoUrls, pet.photoUrls) &&
                Arrays.equals(tags, pet.tags) &&
                status.equals(pet.status);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, category, name, status);
        result = 31 * result + Arrays.hashCode(photoUrls);
        result = 31 * result + Arrays.hashCode(tags);
        return result;
    }
}
