package org.petShop.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Category {
    private int id;
    private String name;

    public Category(){}

    public Category(int categoryID, String categoryName) {
        this.id = categoryID;
        this.name = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return id == category.id &&
                name.equals(category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
