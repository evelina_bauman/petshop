package org.petShop.model;

import lombok.Data;

import java.util.Objects;

@Data
public class Tag {
    private int id;
    private String name;

    public Tag(){}

    public Tag(int tagID, String tag) {
        this.id = tagID;
        this.name = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return id == tag.id &&
                name.equals(tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
